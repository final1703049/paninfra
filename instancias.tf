

# Creamos las instancias, primero la pública
resource "aws_instance" "ec2_publica" {
  ami           = var.ami_instancia
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.publica.id
  key_name = "llaves_publicas"
  associate_public_ip_address = true
 # vpc_security_group_ids = "publico_sg"

  # Conexion
  connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = tls_private_key.llaves_publicas
    host       = self.public_ip
  }
  tags = {
    Name = "Instancia_Publica"
  }
}

# Luego la privada
resource "aws_instance" "ec2_privada" {
  ami           = var.ami_instancia
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.privada.id
  key_name = "llaves_privadas"
  # vpc_security_group_ids = "publico_sg"

  # Conexion
  connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = tls_private_key.llaves_privadas
    host        = self.public_ip
  }

  tags = {
    Name = "Instancia_Privada"
  }
}

## Generacion llaves

#resource "tls_private_key" "llaves_publicas" {
 # algorithm = "RSA"
  #rsa_bits = 2048
#}

#resource "tls_private_key" "llaves_privadas" {
 # algorithm = "RSA"
  #rsa_bits = 2048
#}