Explicacion:

Parte de Red (vpcysubred.tf):

Creo la vpc1 donde se alojaran ambas subreds, ec2 y s3. Le asigno el rango de ip de 10.0.0.0/16 con el recurso "aws_vpc"
Luego, creo la subred publica y privada con recurso "aws_subnet", ambas conectadas a la vpc,
con ip 10.0.1.0/24 y 10.0.2.0/24 respectivamente
Las alojo en las zonas de us-east1a y b.

Despues, creo el internet gateway "aws_internet_Gateway" y lo asocio a la vpc, tambien se crea una ip elastica "aws_eip"
para que tenga salida a internet.
Se crea el nat gateway "aws_nat_gateway" y se asocia a la subnet privada, solo va a poder bajar datos de internet pero no se podrá ver desde afuera

Creo las tablas de ruta para el IGW y el NATGW con aws_route_Table y se crean las rutas con aws_route para ambas subnets

Configuro la ruta de la subred publica hacia internet con el destination cidr block "0.0.0.0/0" Redigrige todo el trafico
con destino a cualquier ip a traves del igw, lo que permite que las instancias salgan a internet.

Luego hago lo mismo para la privada. Despues le asocio las tablas de ruta previamente creadas a cada subred.

Explicacion de Instancias EC2(instancias.tf):

Vamos a crear una instancia para cada subred, uso el recurso aws_instance para esto
Le asigno la ami que es como la imagen del so. Use una variable que defini en variables.tf para esta ami
Luego uso el instance_type t2.micro que es la default que da aws gratuito
Le asigno las llaves que baje desde key managment
Le asigno el security group a ambas instancias para que se puedan conectar por ssh, lo que configure usando 
"connection" , le especifico que es por ssh con "ec2-user" el usuario por defecto, y le linkeo la llave de nuevo.

Debajo comente el codigo para generar las llaves y no me funcionó, las tuve que bajar como marqué anteriormente, desde la interfaz en key managment.

A las instancias me conecte usando el comando de ssh que provee aws.


Explicacion de buckets S3(cubos.tf):

En cubos.tf creo el bucket publico y privado con el recurso "aws_s3_bucket"
Indico que la acces list es privada lo cual no permite que se vean publicamente, a no ser que se indique en otra politica.
Creo las politicas de acceso para ambos buckets con el recurso "aws_s3_bucket_policy"

Luego configuro las politicas en el .json definen los permisos para leer, escribir y eliminar objetos individuales dentro del bucket 
y permito que se puedan comunicar entre si.

Explicacion Variables (variables.tf) :

Configuro unicamente una variable (podrian ser más en la vpc, politicas, etc)
Configuro una variable para llamar el numero de ami con "ami_instancia" que luego uso en la parte de ec2

Explicacion Repositorios (repositorio.tf):

Creo un repositorio de imagenes con el recurso "aws_ecr_repository" y le doy las caracteristicas vistas en el pdf del drive
COnfiguro la politica de acceso para el repositorio...

Explicacion politicas de grupo (grupos.tf):

Creo las politicas de grupo privadas y publicas que se asignan a las ec2 con "aws_security_group"
Les pongo nombre y las linkeo a la vpc. 

Estas politicas permiten conectarse por ssh.



