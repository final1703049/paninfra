# Creación del bucket de S3 público
resource "aws_s3_bucket" "bucket-publico-pan" {
    bucket = "bucket-publico-pan"
    acl    = "private"
}

# Política de acceso para el bucket público
resource "aws_s3_bucket_policy" "bucket_publico_policy" {
  bucket = aws_s3_bucket.bucket-publico-pan.id

  policy = jsonencode({
    Version = "2024-04-23",
    Statement = [
      {
        Effect    = "Allow",
        Principal = "*",
        Action    = [
          "s3:GetObject",
          "s3:PutObject",
          "s3:DeleteObject"
        ],
        Resource  = "${aws_s3_bucket.bucket-publico-pan.arn}/*"
      }
    ]
  })
}

# Creación del bucket de S3 privado
resource "aws_s3_bucket" "bucket-privado-pan" {
    bucket = "bucket-privado-pan"
    acl    = "private"
}

# Política de acceso para el bucket privado
resource "aws_s3_bucket_policy" "bucket_privado_policy" {
  bucket = aws_s3_bucket.bucket-privado-pan.id

  policy = jsonencode({
    Version = "2024-04-23",
    Statement = [
      {
        Effect    = "Allow",
        Principal = "*",
        Action    = [
          "s3:GetObject",
          "s3:PutObject",
          "s3:DeleteObject"
        ],
        Resource  = "${aws_s3_bucket.bucket-privado-pan.arn}/*"
      }
    ]
  })
}