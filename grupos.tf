resource "aws_security_group" "publico_sg" {
  name = "publicosg"
  vpc_id = aws_vpc.vpc1.id
  ingress{
  from_port   = 22
  to_port     = 22
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "privado_sg" {
  name = "privadosg"
  vpc_id = aws_vpc.vpc1.id
  ingress{
  from_port   = 22
  to_port     = 22
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
  }
}
