# Desplegar una VPC
resource "aws_vpc" "vpc1" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "MiVpc"
  }
}

# Desplegar primera subnet, la pública
resource "aws_subnet" "publica" {
  vpc_id            = aws_vpc.vpc1.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "SubnetPublica"
  }
}

# Desplegar segunda subnet
resource "aws_subnet" "privada" {
  vpc_id            = aws_vpc.vpc1.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "SubnetPrivada"
  }
}

# Desplegar el Internet Gateway y asociarlo a la VPC
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc1.id

  tags = {
    Name = "igw_publico"
  }
}

# Asociación de una IP elástica
resource "aws_eip" "eip1" {
  vpc = true
}

# Creamos el NAT Gateway y lo asociamos a la subnet privada
resource "aws_nat_gateway" "natgw" {
  allocation_id = aws_eip.eip1.id
  subnet_id     = aws_subnet.privada.id

  tags = {
    Name = "Natgateway"
  }
}

# Configuramos la tabla de ruta con el Internet Gateway
resource "aws_route_table" "rt_publica" {
  vpc_id = aws_vpc.vpc1.id
}

# Configuramos la tabla de ruta con el NAT Gateway
resource "aws_route_table" "rt_privada" {
  vpc_id = aws_vpc.vpc1.id
}

# Creamos la ruta para la subred pública hacia Internet a través de IGW
resource "aws_route" "acceso_internet" {
  route_table_id         = aws_route_table.rt_publica.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw.id
}

# Creamos la ruta para la subred privada hacia Internet a través de NAT
resource "aws_route" "salida_internet" {
  route_table_id         = aws_route_table.rt_privada.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.natgw.id
}

# Asociamos la tabla de ruteo de la subred pública
resource "aws_route_table_association" "asociacion_publica" {
  subnet_id      = aws_subnet.publica.id
  route_table_id = aws_route_table.rt_publica.id
}

# Asociamos la tabla de ruteo de la subred privada
resource "aws_route_table_association" "asociacion_privada" {
  subnet_id      = aws_subnet.privada.id
  route_table_id = aws_route_table.rt_privada.id
}